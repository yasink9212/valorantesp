void ESP() {

	int revise = 0;
	auto entityListCopy = entityList;
	float distance = 0.f;

	if (entityListCopy.empty())
	{
		return;
	}

	for (unsigned long i = 0; i < entityListCopy.size(); ++i)
	{
		TslEntity entity = entityListCopy[i];
		bool is_alive = read<bool>(damage_controller + 0x425);
		health = read<float>(damage_controller + 0x1B0);

		if (entity.pObjPointer == local_player_pawn)
		{
			continue;
		}

		if (entity.pObjPointer == is_alive)
		{
			continue;
		}

		

		camera_position = read<Vector3>(camera_manager + 0x1260);

		camera_rotation = read<Vector3>(camera_manager + 0x126C);
		camera_fov = read<float>(camera_manager + 0x4B0) + 33.0f;

		auto root_component = read<std::uint64_t>(entity.pObjPointer + 0x238);
		auto RootPos = read<Vector3>(root_component + 0x1E0);

		wchar_t pPlayerName[64] = { NULL };

		auto ActorPlayerState = read<uint64_t>(entity.pObjPointer + 0x4B0);

		readwtf(ActorPlayerState + 0x490, &pPlayerName, sizeof(pPlayerName));

		uint64_t mesh = entity.mesh;

		distance = LocalPos.Distance(RootPos) / 100.f;

		if (mesh != 0x00)
		{
			Vector3 vHeadBone = GetBoneWithRotation(mesh, 8);
			Vector3 RootPos2d = WorldToScreen(RootPos, camera_position, camera_rotation, camera_fov);

			sprintf(drawBuff, u8" %s %0.fm ", pPlayerName, distance);

			if (vHeadBone.z <= RootPos.z)
				continue;

			revise = strlen(drawBuff) * 7 + 28;
			DrawFilledRect(RootPos2d.x - (revise / 2) + (Rect.w / 2) - 1, RootPos2d.y + Rect.z - 25, revise + 1, 3, &Col.blue);
			DrawFilledRect(RootPos2d.x - (revise / 2) + (Rect.w / 2) - 1, RootPos2d.y + Rect.z - 28, revise + 1, 16, &Col.blue);
			DrawNewText(RootPos2d.x - (revise / 2) + (Rect.w / 2) + 21, RootPos2d.y + Rect.z - 27, &Col.white_, drawBuff);

			if (health >= 100)
				health = 100;
			int length = (int)(health * revise / 100);

			if (health >= 80)
				DrawFilledRect(RootPos2d.x - (revise / 2) + (Rect.w / 2), RootPos2d.y + Rect.z - 13, length, 1, &Col.greens_);
			else if (health < 80 && health > 50)
				DrawFilledRect(RootPos2d.x - (revise / 2) + (Rect.w / 2), RootPos2d.y + Rect.z - 13, length, 1, &Col.red_);
			else if (health <= 50)
				DrawFilledRect(RootPos2d.x - (revise / 2) + (Rect.w / 2), RootPos2d.y + Rect.z - 13, length, 1, &Col.gray_);
		}
	}
}